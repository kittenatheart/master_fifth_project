﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using TMPro;
using System.Security.Cryptography;

public class PlayerController : MonoBehaviour
{
	Rigidbody2D rB2D;
	private int count = 0;
	public static int isCaught = 1;

	public float runSpeed;
	public float jumpForce;

	public SpriteRenderer spriteRenderer;
	public Animator animator;

	public TextMeshProUGUI countText;
	public GameObject winTextObject;


	void Start()
	{
		rB2D = GetComponent<Rigidbody2D>();
		setCountText();
		winTextObject.SetActive(false);
	}

	void Update()
	{
		
	}

	void FixedUpdate()
	{
		float horizontalInput = Input.GetAxis("Horizontal");
		float verticalInput = Input.GetAxis("Vertical");
		rB2D.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, verticalInput *runSpeed * Time.deltaTime);

		if(rB2D.velocity.x>=1)
        {
			spriteRenderer.flipX = false;
        }
        else
        {
			spriteRenderer.flipX = true;
			
        }
		
		if (Mathf.Abs(horizontalInput)>0f || Mathf.Abs(verticalInput) > 0f)
        {
			animator.SetBool("isRunning", true);
        }
        else
        {
			animator.SetBool("isRunning", false);
		}
		/*if (rB2D.velocity.y > 1)
		{
			animator.SetBool("isRunningUp", true);
			animator.SetBool("isRunningDown", false);
		}
		else
		{
			animator.SetBool("isRunningUp", false);
			animator.SetBool("isRunningDown", true);
		}*/
	}


	void OnTriggerEnter2D(Collider2D other)
    {
		if (other.CompareTag("Pickup"))
        {
			isCaught += 1;
			other.GetComponent<SpriteRenderer>().enabled = false;
			count += 1;
			setCountText();
		}
		else if(other.CompareTag("Door"))
        {
			Destroy(other);
		}
	}

	void setCountText()
	{
		countText.text = "Count: " + count.ToString() + "/3";
		if (count == 3)
		{
			winTextObject.SetActive(true);
		}
	}

	/* NO LONGER NEEDED - this game is no longer a platformer
	void Jump()
	{
		rB2D.velocity = new Vector2(rB2D.velocity.x, jumpForce);
	}
	if (Input.GetButtonDown("Jump"))
		{
			int levelMask = LayerMask.GetMask("Level");
			if (Physics2D.BoxCast(transform.position, new Vector2(1f, .1f), 0f, Vector2.down, 0.1f, levelMask))
			{
				Jump();
			}
		}
	*/
}

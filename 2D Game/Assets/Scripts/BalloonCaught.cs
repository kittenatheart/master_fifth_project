﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalloonCaught : MonoBehaviour
{
    public Animator animator;
    private int caught;
    private SpriteRenderer spriteRenderer;

    void Start()
    {
        caught = PlayerController.isCaught;
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if(caught == 2)
        {
            animator.SetBool("isTouched", true);
            float counter = 0f;
            while (counter < 16f)
            {
                counter += Time.deltaTime;
            }
            spriteRenderer.enabled = false;
            PlayerController.isCaught -= 1;
        }
    }
}
